package dfrc.xplanetcpinterface;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.http.conn.util.InetAddressUtils;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity implements OnClickListener {
    /** The port the server will listen on */
    private static final int port = 1234;
    /** The tag used in debugging messages. Typically the class name */
    private static final String TAG = "MainActivity";

    // Variables to display on screen
    private int time;
    private double latitude;
    private double longitude;
    private double altitude;
    private float ktas;
    private float kias;
    private float bank;
    private float climbRate;
    private float rollRate;
    private float heading;
    private float windVelN;
    private float windVelE;

    // Objects that start the server and read the data
    ServerSocket serverSocket;
    DataInputStream input;
    Socket socket;

    // Create Buttons
    ToggleButton serverButton;

    // Create the EditTexts
    private EditText timeEdit;
    private EditText latitudeEdit;
    private EditText longitudeEdit;
    private EditText altitudeEdit;
    private EditText ktasEdit;
    private EditText kiasEdit;
    private EditText bankEdit;
    private EditText climbEdit;
    private EditText rollEdit;
    private EditText headingEdit;
    private EditText windNorEdit;
    private EditText windEastEdit;

    // Create TextViews
    private TextView portView;
    private TextView ipView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serverButton = (ToggleButton) findViewById(R.id.serverButton);
        serverButton.setOnClickListener(this);

        // Set the EditTexts
        timeEdit = (EditText) findViewById(R.id.timeEdit);
        latitudeEdit = (EditText) findViewById(R.id.latitudeEdit);
        longitudeEdit = (EditText) findViewById(R.id.longitudeEdit);
        altitudeEdit = (EditText) findViewById(R.id.altitudeEdit);
        ktasEdit = (EditText) findViewById(R.id.ktasEdit);
        kiasEdit = (EditText) findViewById(R.id.kiasEdit);
        bankEdit = (EditText) findViewById(R.id.bankEdit);
        climbEdit = (EditText) findViewById(R.id.climbEdit);
        rollEdit = (EditText) findViewById(R.id.rollEdit);
        headingEdit = (EditText) findViewById(R.id.headingEdit);
        windNorEdit = (EditText) findViewById(R.id.windNorEdit);
        windEastEdit = (EditText) findViewById(R.id.windEastEdit);

        // Set the TextViews
        portView = (TextView) findViewById(R.id.portView);
        portView.setText(Integer.toString(port));
        ipView = (TextView) findViewById(R.id.ipView);
        ipView.setText(getIPAddress(true));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            // Make sure the socket is closed when the app is closed
            serverSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Close socket in onDestroy: " + e );
        }
    }

    /* No options menu for now
     * 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.serverButton:
                if (serverButton.isChecked()) {
                    // Start new task to create a server
                    new socketListenerTask().execute();
                } else {
                    // Clearing the screen shows the user that it has stopped listening
                    clearScreen();
                }
                break;
        }
    }

    /**
     * Get IP address from first non-localhost interface
     * @param ipv4  true=return ipv4, false=return ipv6
     * @return  address or error string
     *  Adapted from http://silverballsoftware.com/get-your-ip-address-android-code
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase(Locale.US);
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                return delim<0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Get IP: " + e);
        }
        return "Unknown IP";
    }

    /**
     * This method clears all of the EditTexts in the display.
     */
    private void clearScreen() {
        timeEdit.setText("");
        latitudeEdit.setText("");
        longitudeEdit.setText("");
        altitudeEdit.setText("");
        ktasEdit.setText("");
        kiasEdit.setText("");
        bankEdit.setText("");
        climbEdit.setText("");
        rollEdit.setText("");
        headingEdit.setText("");
        windNorEdit.setText("");
        windEastEdit.setText("");
    }

    /**
     * This task opens a ServerSocket and listens on the port defined above. It accepts a connection from
     * a Python script that sends data in the form ('> I d d d f f f f f f f f'). It also updates the screen and
     * displays the variables read in their respective EditTexts.
     */
    private class socketListenerTask extends AsyncTask<Void, Integer, Void>{
        @Override
        protected void onPreExecute() {
            // update the UI immediately after the task is executed
            super.onPreExecute();
            Toast.makeText(MainActivity.this,
                    "Listening...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i(TAG, "socketListenThread called");
            try {
                serverSocket = new ServerSocket(port);
                Log.i(TAG, "Server waiting for client on port " +
                        serverSocket.getLocalPort());
                socket = serverSocket.accept();
                Log.i(TAG, "New connection accepted " +
                        socket.getInetAddress() +
                        ":" + socket.getPort());
                input = new DataInputStream(socket.getInputStream());
                // print received data
                try {
                    while(true) {
                        if (!serverButton.isChecked()) {
                            break;
                        }
                        if (input.available() > 0) {
                            time = input.readInt();
                            latitude = input.readDouble();
                            longitude = input.readDouble();
                            altitude = input.readDouble();
                            ktas = input.readFloat();
                            kias = input.readFloat();
                            bank = input.readFloat();
                            climbRate = input.readFloat();
                            rollRate = input.readFloat();
                            heading = input.readFloat();
                            windVelN = input.readFloat();
                            windVelE = input.readFloat();
                            publishProgress(1);
                        } else { }
                    }
                }
                catch (IOException e) {
                    Log.e(TAG, "Read data: " + e );
                }
            }
            catch (Exception e ) {
                Log.e(TAG, "Open server: " + e );
                try {
                    serverSocket.close();
                } catch (IOException e2) {
                    Log.e(TAG, "Close server in doInBackground: " + e );
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            timeEdit.setText(Integer.toString(time));
            latitudeEdit.setText(Double.toString(latitude));
            longitudeEdit.setText(Double.toString(longitude));
            altitudeEdit.setText(Double.toString(altitude));
            ktasEdit.setText(Float.toString(ktas));
            kiasEdit.setText(Float.toString(kias));
            bankEdit.setText(Float.toString(bank));
            climbEdit.setText(Float.toString(climbRate));
            rollEdit.setText(Float.toString(rollRate));
            headingEdit.setText(Float.toString(heading));
            windNorEdit.setText(Float.toString(windVelN));
            windEastEdit.setText(Float.toString(windVelE));
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                socket.close();
                serverSocket.close();
                Toast.makeText(MainActivity.this,
                        "Closing server...", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Log.e(TAG, "Close socket: " + e );
                Toast.makeText(MainActivity.this,
                        "Unable to close server", Toast.LENGTH_SHORT).show();
            }
            // Always uncheck the button. If there is an error and serverSocket gets closed in
            // doInBackground, then the user needs to press the button again to reopen the server
            serverButton.setChecked(false);
        }
    }
}